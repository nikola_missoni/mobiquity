package com.mobiquityinc.packer;

import static org.junit.Assert.*;

import org.junit.Test;

import com.mobiquityinc.model.Item;

public class ItemTest {

	@Test
	public void testGetWeight() {
		Item item = new Item(1, 25.55, 50);
		
		assertEquals(2555, item.getWeight());
	}

}
