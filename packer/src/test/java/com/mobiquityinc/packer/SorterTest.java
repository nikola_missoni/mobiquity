package com.mobiquityinc.packer;

import static org.junit.Assert.*;

import org.junit.Test;

import com.mobiquityinc.model.Item;
import com.mobiquityinc.util.Sorter;

public class SorterTest {

	@Test
	public void testCompareFirstHeavierThanSecond() {
		Item i1 = new Item(6, 48.77, 79);
		Item i2 = new Item(8, 19.36, 79);
		
		int compareVal = new Sorter().compare(i1, i2);

		assertTrue(compareVal > 0);
	}
	
	@Test
	public void testCompareSecondHeavierThanFirst() {
		Item i1 = new Item(6, 19.36, 79);
		Item i2 = new Item(8, 48.77, 79);
		
		int compareVal = new Sorter().compare(i1, i2);

		assertTrue(compareVal < 0);
	}
	
	@Test
	public void testCompareSameWeight() {
		Item i1 = new Item(6, 19.36, 79);
		Item i2 = new Item(8, 19.36, 79);
		
		int compareVal = new Sorter().compare(i1, i2);

		assertTrue(compareVal == 0);
	}

}
