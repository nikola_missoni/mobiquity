package com.mobiquityinc.packer;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.Ignore;
import org.junit.Test;

import com.mobiquityinc.exception.APIException;
import com.mobiquityinc.util.Util;

public class UtilTest {

	@Ignore
	@Test
	public void testReadFromFile() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetCapacity() {
		String line = "81 : (1,53.38,€45) (2,88.62,€98) (3,78.48,€3) (4,72.30,€76) (5,30.18,€9) (6,46.34,€48)";
		try {
			var capacity = Util.getCapacity(line);
			assertEquals(81, capacity);
		} catch (APIException e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testGetCapacityNegative() {
		String line = "-81 : (1,53.38,€45) (2,88.62,€98) (3,78.48,€3) (4,72.30,€76) (5,30.18,€9) (6,46.34,€48)";
		try {
			Util.getCapacity(line);
			fail("Expected APIException");
		} catch (APIException e) {
			assertTrue(e.getMessage().equalsIgnoreCase("Capacity can not be less than zero"));
		}
	}
	
	@Test
	public void testGetCapacityOverWeightLimit() {
		String line = "181 : (1,53.38,€45) (2,88.62,€98) (3,78.48,€3) (4,72.30,€76) (5,30.18,€9) (6,46.34,€48)";
		try {
			Util.getCapacity(line);
			fail("Expected APIException");
		} catch (APIException e) {
			assertEquals( "Capacity 181 exceeds maximum allowed package weight", e.getMessage());
		}
	}
	
	@Test
	public void testGetCapacityNumberFormatException() throws APIException {
		String line = "a : (1,53.38,€45) (2,88.62,€98) (3,78.48,€3) (4,72.30,€76) (5,30.18,€9) (6,46.34,€48)";
		
		try {
			Util.getCapacity(line);
			fail("Expected NumberFormatException");
		} catch (APIException e) {
			assertTrue(e.getMessage().equals("Missing or wrong capacity format"));
		}
	}
	
	@Test
	public void testGetCapacityMissingColumn() throws APIException {
		String line = "81 (1,53.38,€45) (2,88.62,€98) (3,78.48,€3) (4,72.30,€76) (5,30.18,€9) (6,46.34,€48)";
		
		try {
			Util.getCapacity(line);
			fail("Expected APIException");
		} catch (APIException e) {
			assertTrue(e.getMessage().equalsIgnoreCase("Missing column separator"));
		}
	}
	
	@Test
	public void testGetCapacityMissingCapacity() throws APIException {
		String line = " : (1,53.38,€45) (2,88.62,€98) (3,78.48,€3) (4,72.30,€76) (5,30.18,€9) (6,46.34,€48)";
		
		try {
			Util.getCapacity(line);
			fail("Expected APIException");
		} catch (APIException e) {
			assertTrue(e.getMessage().equals("Missing or wrong capacity format"));
		}
	}

	@Test
	public void testGetItems() throws APIException {
		String line = "81 : (1,53.38,€45) (2,88.62,€98) (3,78.48,€3) (4,72.30,€76) (5,30.18,€9) (6,46.34,€48)";
		line = line.replace("€", "");
		var items = Util.getItems(line);
		
		assertEquals(6, items.length);
	}
	
	@Test
	public void testGetItemsMissingItems() throws APIException {
		String line = "81 : ";
		line = line.replace("€", "");
		
		try {
			Util.getItems(line);
			fail("Expect APIException");
		} catch(APIException e) {
			assertTrue(e.getMessage().equalsIgnoreCase("Missing item data"));
		}
	}

}
