package com.mobiquityinc.packer;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;
import static org.powermock.api.mockito.PowerMockito.mockStatic;

import java.util.ArrayList;
import java.util.Arrays;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.mobiquityinc.exception.APIException;
import com.mobiquityinc.model.Item;
import com.mobiquityinc.util.Util;

@RunWith(PowerMockRunner.class)
@PrepareForTest(Util.class)
public class PackerTest {

	@Test
	public void testPack() throws APIException {

		ArrayList<String> lines = new ArrayList<>(Arrays.asList(
				//"81 : (1,53.38,€45) (2,88.62,€98) (3,78.48,€3) (4,72.30,€76) (5,30.18,€9) (6,46.34,€48)"//,
				//"8 : (1,15.3,34)"//,
				//"75 : (1,85.31,€29) (2,14.55,€74) (3,3.98,€16) (4,26.24,€55) (5,63.69,€52) (6,76.25,€75) (7,60.02,€74) (8,93.18,€35) (9,89.95,€78)",
				"56 : (1,90.72,€13) (2,33.80,€40) (3,43.15,€10) (4,37.97,€16) (5,46.81,€36) (6,48.77,€79) (7,81.80,€45) (8,19.36,€79) (9,6.76,€64)"
				));

		Item[] items = {new Item(1,90.72,13), new Item(2,33.80,40), new Item(3,43.15,10), new Item(4,37.97,16),
						new Item(5,46.81,36), new Item(6,48.77,79), new Item(7,81.80,45), new Item(8,19.36,79), new Item(9,6.76,64)};
		
		mockStatic(Util.class);
		
		try {
			//Mocked, return values defined in test
			when(Util.readFromFile("test.txt")).thenReturn(lines);
			//Mocked, intercepted call to method, returns defined
			//Unit tested in separate test case
			when(Util.getCapacity(lines.get(0))).thenReturn(56);
			when(Util.getItems(lines.get(0))).thenReturn(items);
			
			String result = Packer.pack("test.txt");
			
			assertEquals("8 9", result);
		}
		catch(APIException ex) {}
	}
}
