package com.mobiquityinc.model;

import static com.mobiquityinc.constants.Constants.CONVERSION_COEFFICIENT;

public class Item {
	private int index;
	private int weight;
	private int value;

	public Item(int index, double weight, int value) {
		this.index = index;
		this.weight = (int) (weight * CONVERSION_COEFFICIENT);
		this.value = value;
	}

	public int getIndex() {
		return index;
	}

	public int getWeight() {
		return weight;
	}

	public int getValue() {
		return value;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + index;
		result = prime * result + value;
		result = prime * result + weight;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Item other = (Item) obj;
		if (index != other.index)
			return false;
		if (value != other.value)
			return false;
		if (weight != other.weight)
			return false;
		return true;
	}
}
