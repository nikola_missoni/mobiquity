package com.mobiquityinc.packer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

import com.mobiquityinc.exception.APIException;
import com.mobiquityinc.model.Item;
import com.mobiquityinc.util.Sorter;
import com.mobiquityinc.util.Util;

import com.mobiquityinc.constants.Constants;

/**
 * @author Nikola Missoni
 * Main class with business logic for package problem
 */
public class Packer {

	public static String pack(String filePath) throws APIException{
		var capacity = 0;
		Item[] items = null;
		int[][] matrix;
		StringBuilder stringBuilder = new StringBuilder();
		
		var linesFromFile = Util.readFromFile(filePath);
		
		for (String line : linesFromFile) {
			capacity = Util.getCapacity(line) * Constants.CONVERSION_COEFFICIENT;
			items = Util.getItems(line);
			Arrays.sort(items, new Sorter());
			
			matrix = calculateMaxValMatrix(items, capacity);

			ArrayList<Item> arrayList = new ArrayList<Item>(Arrays.asList(items));
			for(Item item : getSolutionItems(arrayList, capacity, arrayList.size(), matrix)) {
				stringBuilder.append(item.getIndex()).append(" ");
			}
			stringBuilder.append(System.lineSeparator());
		}
		
		return stringBuilder.toString().trim();
	}

	private static int[][] calculateMaxValMatrix(Item[] listOfItems,int capacity) {
		int numberOfItems = listOfItems.length;
		int[][] matrix = new int[numberOfItems+1][capacity+1];
		
		//first line set to 0s
		for(int i = 0; i <= capacity; i++) {
			matrix[0][i] = 0;
		}
		
		for(int i = 1; i <= numberOfItems; i++) {
			for(int j = 0; j <= capacity; j++) {
				if(listOfItems[i-1].getWeight() > j) {
					matrix[i][j] = matrix[i-1][j];
				}
				else {
					matrix[i][j] = Math.max(matrix[i-1][j], matrix[i-1][j - listOfItems[i-1].getWeight()] + listOfItems[i-1].getValue());
				}
			}
		}
		return matrix;
	}

	private static List<Item> getSolutionItems(ArrayList<Item> listOfItems, int capacity, int numberOfItems, int[][] table) {
		int res = table[numberOfItems][capacity];

		int w = capacity;

		List<Item> itemSolution = new ArrayList<>();

		for (int i = numberOfItems; i > 0 && res > 0; i--) {
			if (res != table[i - 1][w]) {
				itemSolution.add(listOfItems.get(i - 1));
				res -= listOfItems.get(i - 1).getValue();
				w -= listOfItems.get(i - 1).getWeight();
			}
		}
		
		itemSolution.sort(Comparator.comparing(Item::getIndex));
		return itemSolution;
	}
}
