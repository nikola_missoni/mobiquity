package com.mobiquityinc.exception;

public class APIException extends Exception {

	/**
	 * @author Nikola Missoni
	 * Mobiquity APIException implementation
	 */
	private static final long serialVersionUID = 5199629890017767754L;
	
	public APIException(String message) {
		super(message);
	}

}
