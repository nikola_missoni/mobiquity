package com.mobiquityinc.constants;

/**
 * @author Nikola Missoni
 * Util class to hold constants
 */
public final class Constants {

	//Hide constructor
	private Constants() {}
	
	//Coefficient used to convert kg to g
	public static final int CONVERSION_COEFFICIENT = 100;
	
	public static final int MAX_CAPACITY = 100;
}
