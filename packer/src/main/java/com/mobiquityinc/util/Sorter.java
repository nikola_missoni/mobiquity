package com.mobiquityinc.util;

import java.util.Comparator;

import com.mobiquityinc.model.Item;

/**
 * @author Nikola Missoni
 * Custom sorter to sort Item array
 */
public class Sorter implements Comparator<Item> {

//	Because it is more profitable to fill all possible volume with substance having the lowest value/weight ratio. 
//	So use up all given quantity of item with the best value/weight ratio, then with the second and so on.
	@Override
	public int compare(Item o1, Item o2) {
		return (o1.getWeight() / o1.getValue()) - (o2.getWeight() / o2.getValue());
	}

}
