package com.mobiquityinc.util;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.mobiquityinc.exception.APIException;
import com.mobiquityinc.model.Item;
import static com.mobiquityinc.constants.Constants.MAX_CAPACITY;;

/**
 * @author Nikola Missoni
 * Class with static methods used to read from file, generate Item and
 * get Capacity
 */
public class Util {

	public static ArrayList<String> readFromFile(String filePath) throws APIException{
		List<String> linesFromFile = new ArrayList<>();
		
		try(Stream<String> stream = Files.lines(Paths.get(filePath))){
			linesFromFile = stream.map(line -> line.replaceAll("€", "")).collect(Collectors.toList());
		}
		catch(IOException ex) {
			throw new APIException("File not found");
		}
		return (ArrayList<String>) linesFromFile;
	}
	
	public static int getCapacity(String line) throws APIException {
		var capacity = 0;
		try {
			if(!line.contains(":")) throw new APIException("Missing column separator");
			capacity = Integer.valueOf(line.substring(0, line.indexOf(":")).trim());
			if(capacity >= MAX_CAPACITY) {
				throw new APIException("Capacity " + capacity + " exceeds maximum allowed package weight");
			}else if(capacity < 0) {
				throw new APIException("Capacity can not be less than zero");
			}
		}
		catch(NumberFormatException ex) {
			throw new APIException("Missing or wrong capacity format");
		}
		return capacity;
	}
	
	public static Item[] getItems(String line) throws APIException {
		int counter = 0;
		Item[] items;
		var splittedLine = line.split(":");
		var allItemsData = splittedLine[1].trim().split(" ");
		items = new Item[allItemsData.length];

		for (String item : allItemsData) {
			if(item.equals("")) throw new APIException("Missing item data");
			var itemData = item.replace("(", "").replace(")", "").split(",");
			Item it = new Item(Integer.valueOf(itemData[0]), 
								Double.valueOf(itemData[1]),
								Integer.valueOf(itemData[2]));
			items[counter] = it;
			counter++;
		}
		return items;
	}
}
