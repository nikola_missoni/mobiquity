Repository with the solution for Mobiquity code challenge.

Solution is implementation of KnapSack problem with add functionality for reading data from file and constructing Item objects from it.
One thing to take in consideration was a constraint to get item that weights less if there are two that satisfie problem solution.

Also, there was an issue with decimal representation of weight, which I solved by changing the unit (e.g. kg -> gr) for weight and for capacity and 
that enabled me to use values as integers.

It was tested with data provided in task description.